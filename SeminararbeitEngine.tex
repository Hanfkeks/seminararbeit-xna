\documentclass[
11pt, 
a4paper, 
oneside, 
headinclude,footinclude,
BCOR5mm,
]{scrartcl}

\input{structure.tex}
\providecommand{\HasFlash}{false}
\newcommand{\mediacontent}[2]{\ifthenelse{\equal{\HasFlash}{true}}{#1}{#2}}

\title{\normalfont\spacedallcaps{Entwicklung einer 2D-Engine mit \CSharp \ und XNA}} 

\author{\spacedlowsmallcaps{Michael Höpp}}


\begin{document}


\renewcommand\lstlistlistingname{Codebeispiele}
\renewcommand{\lstlistingname}{Codebeispiel}

\renewcommand{\sectionmark}[1]{\markright{\spacedlowsmallcaps{#1}}}
\lehead{\mbox{\llap{\small\thepage\kern1em\color{halfgray} \vline}\color{halfgray}\hspace{0.5em}\rightmark\hfil}}

\pagestyle{scrheadings} 

\includepdf{Schmutztitel.pdf}

%\includegraphics[]{Schmutztitel}
\clearpage
\setcounter{page}{1}

\maketitle

\setcounter{tocdepth}{2}

\tableofcontents 
\newpage
\listoffigures

\lstlistoflistings



\newpage


\section{Einleitung}

Der Anreiz als Privatperson ein Spiel zu entwickeln kann unterschiedlich sein. Ob kommerzielle Motivation, der Reiz etwas neues zu Erschaffen, oder einfach nur der Spaß am Programmieren, in der Planungsphase stellt sich immer die gleiche Frage: Welche Technologie soll der Grundstein für das Spiel sein? Besonders im Bereich der selbständigen Entwickler ist es beliebt zweidimensionale Spiele im retro Look zu Programmieren. Hierfür gibt es viele fertige Engines, die dem Nutzer ein bereits fertiges System für Rendering, Physik, Kollision und vielem mehr bereitstellen. Einarbeitungszeit und eine gewisse Bereitschaft, sich auf die Programmier- oder Skriptsprache der Engine einzulassen sind erforderlich. In vielen Fällen hilft dabei eine \klammer{Learning by Doing} Mentalität, die aber gleichzeitig Probleme schafft, wie unsauberen und schlecht wartbaren Code oder schwer nachvollziehbare Bugs. Oftmals arbeitet man mehr gegen die Engine, als mit ihr und verbringt zu viel Zeit auf Hilfeforen und Tutorial Seiten. Aus diesen Gründen ist es oft ratsam so viel wie möglich selbst zu implementieren. Dies kann ebenfalls viel Zeit und Mühe in Anspruch nehmen und genau hierbei soll Ihnen dieser Artikel helfen, um eine performante, wiederverwendbare, modifizierbare und nachvollziehbare 2D Engine für Ihre zukünftigen Projekte zu entwickeln.

\section{XNA}

Das von Microsoft entwickelte Framework zur Spieleentwicklung XNA vereint viele kleinere native Frameworks und wrappt diese für die Programmiersprache \CSharp. Die von XNA unterstützten Platformen sind dabei Windows Phone 7, Xbox 360 und PC mit DirectX 9 oder höher.\footcite[Vgl.][3]{Reed.2011} Die wichtigsten in XNA enthaltenen Frameworks sind:

	\begin{itemize}
		\item {DirectX: Hochoptimierte Grafikbibliothek}
		\item {XACT: Hardwarebeschleunigte Soundbibliothek}
		\item {XInput: Eingabebibliothek für verschiedenste Eingabegeräte}
	\end{itemize}

\CSharp ist eine Objektorientierte Programmiersprache, die Java von der Syntax stark ähnelt, aber einige zusätzliche Features bietet. Die Sprache basiert auf dem .NET Framework, kann aber auch mit dem Mono Framwork für Linux kompiliert werden. Die Linux Alternative von XNA heißt Monogame und ist zum Großteil identisch, diese wird aber immer noch periodisch upgedatet und verändert. Für gewöhnlich ist der Einstieg in \CSharp sehr leicht, wenn man bereits mit Java oder C\texttt{++} vertraut ist. In diesem Artikel wird jedoch oft das \klammer{Property} Konstrukt benutzt, das in diesen beiden Programmiersprachen nicht vorhanden ist. Properties sind ein Ersatz für Getter und Setter und werden genau so aufgerufen wie gewöhnliche Variablen. Dies ermöglicht einen sehr sauberen Programmierstil, da der Code nicht mit unnötigen Getter und Setter Methoden verschmutzt werden muss, stattdessen können gewöhnliche Variablen direkt als öffentlich markiert werden. Wenn später Bedarf an einer spezielleren Logik zum Setzen oder Auslesen der Werte besteht, kann die Variable in eine Property umgewandelt werden, ohne dass Änderungen irgendwo anders im Code, wo diese alte Variable benutzt wird nötig ist. Das Prinzip lässt sich gut an folgender Beispielklasse erkennen:

\begin{lstlisting}[caption=Property,label=lstng:property]
public class MetricUnit
{
	//Regular Variable:	
	public double Meters;
	
	//Property:
	public double KiloMeters
	{
		//Getter:
		get{return Meters/1000;}
		//Setter, assigned value contained in value keyword:
		set{Meters = value*1000;}
	}
}
\end{lstlisting}

Ein weiterer großer Vorteil von \CSharp ist das mächtige .NET Framework im Hintergrund. Ähnlich wie in Java gibt es eine Menge an Klassenbibliotheken, die dem Programmierer zu Verfügung stehen. \CSharp besitzt eine automatische Speicherverwaltung mit \klammer{Garbage Collection}, die von Zeit zu Zeit nicht referenzierte Objekte löscht. Muss Code extrem stark optimiert werden, können auch teile des Codes nativ in C\texttt{++} geschrieben und dann über \CSharp aufgerufen werden, oder umgekehrt.

\subsection{Parallelisiertes Rendering}

XNA ermöglicht schnelles und paralleles Rendering von Grafiken mithilfe eines GraphicDevice Objekts, das als Schnittstelle zur eigentlichen physikalischen Grafikkarte dient\footcite[Vgl.][21]{Reed.2011} und SpriteBatch Objekten, die bequemes zeichnen von Texturen auf den Bildschirm oder andere Ziele ermöglicht, ohne dass sich der Programmierer mit DirectX auseinandersetzen muss. Vor dem Benutzen eines SpriteBatches muss die \klammer{Begin} Methode aufgerufen werden, um die Grafikkarte zu konfigurieren, danach kann die \klammer{Draw} Methode beliebig oft aufgerufen werden. Diese sendet die Sprite Daten an die Grafikkarte\footcite[Vgl.][27]{Reed.2011}. Das eigentliche Rendern ist aber nach dem Draw Aufruf nicht fertig, stattdessen läuft dies im Hintergrund weiter, während parallel dazu weitere derartige Aufrufe gestartet werden können. Dies ist ein signifikanter Performance Vorteil gegenüber sequentiellem Rendering, bei dem jedes mal gewartet wird, bis die Grafik fertig gezeichnet ist. Der \klammer{Draw} Methode können mehrere Parameter übergeben werden, um die Grafik modifiziert zu zeichnen\footcite[Table 3-2][32]{Reed.2011}:
\begin{itemize}
	\item{Texture: Die Grafik, die gezeichnet werden soll}
	\item{Position: Die Bildschirmposition an der die Grafik gezeichnet werden soll}
	\item{SourceRectangle: Optionales Rechteck, um nur einen teil der Grafik zu zeichnen}
	\item{Color: Farbwert, der mit jedem Pixel der Grafik multipliziert wird}
	\item{Rotation: Rotation im Radiant Format}
	\item{Origin: Mittelpunkt der Rotation der Grafik}
	\item{Scale: Skalierungsfaktor}
	\item{Effects: Sprite Effekt, beispielsweise zum Spiegeln}
	\item{LayerDepth: Z-Wert zwischen 0 und 1 um bei mehreren Sprites zu bestimmen, welches über dem anderen dargestellt wird}
\end{itemize}
Nach allen Draw Aufrufen dient die End Methode der SpriteBatch Klasse dazu, sicherzustellen, dass alle Prozesse auf der Grafikkarte abgeschlossen sind und das fertige Bild dargestellt werden kann.


\section{Zentrale Elemente einer 2D Engine}

2D Spiele gibt es in den unterschiedlichsten Formen mit sehr unterschiedlichen Spielelementen, jedoch gibt es bestimmte wiederkehrende Elemente, die von einer Spieleengine effizient und mit möglichst wenig zusätzlichem Programmieraufwand abgedeckt werden können. Jedes Spiel hat einen sogenannten \klammer{Gameloop}, also eine Schleife in dem die Eingaben des Spielers verarbeitet werden, die Spiellogik darauf reagiert und eine meist grafische Ausgabe liefert, solange bis ein Endzustand erreicht ist, also entweder das Spiel gewonnen oder verloren ist, oder der Spieler das Programm manuell beendet. Der Spielzustand und der Gameloop sollten global erreichbar sein, um einen übersichtlichen und simplen Zugriff zu gewähren. Zur Implementierung bietet sich entweder eine statische Klasse oder das Singleton Schema an. In folgenden Codebeispielen werden aus Gründen der Übersichtlichkeit und Aufgabentrennung zwei Klassen definiert: Das Singleton \klammer{EngineMain}, das den Gameloop sowie einige Hilfsmethoden zum Rendering beinhaltet, sowie die statische Klasse \klammer{Board}, das den Spielzustand mit allen Entities und Tiles (mehr dazu im folgenden Kapitel) beinhaltet.

\subsection{Entity- und Tilesystem}

In den meisten 2D Spielen lassen sich die Spielobjekte in Entities und Tiles unterteilen. Tiles sind im Raster nahtlos angeordnete, quadratische Felder. Entities sind Objekte, die sich frei über das Spielfeld bewegen können, ohne zwingend an das Raster der Tiles gebunden zu sein.
\begin{wrapfigure}{r}{6.5cm}
	
	\includegraphics[width=6.5cm]{SuperMario}
	\caption{\footnotesize{Spielszene aus Super Mario mit Tiles in Rot und Entities in Grün markiert}}\label{fig:1}
\end{wrapfigure} 

Obwohl die meisten 2D Spiele sowohl aus Entities, als auch aus Tiles bestehen, gibt es auch Spiele, die nur mit Tiles umgesetzt werden können - wie beispielsweise Snake - oder Spiele die nur mit Entities umgesetzt werden können - zum Beispiel Asteroids. Trotzdem ist es ratsam eine Engine immer so mächtig wie möglich umzusetzen, um denjenigen, die diese Engine später benutzen um ein Spiel zu entwickeln eine große Bandbreite an Optionen zur Umsetzung zu bieten.
Die Tile Klasse benötigt mindestens folgende Informationen:
\begin{itemize}
	\item {Coordinates: Punktkoordiante die die Position des Tiles im Raster angibt}
	\item {Entities: Liste mit Entities deren Zentrum sich auf diesem Tile befindet}
	\item {Texture: Quadratische Grafik die den vollen Raum des Tiles ausfüllt}
\end{itemize}
Zusätzlich dazu ist es empfehlenswert entweder durch einen boolschen Wert in der Tile Klasse abzuspeichern, ob dieses Feld passierbar ist oder nicht. Für komplexere Logik von Wänden und anderen Strukturen sollte stattdessen eine weitere Klasse implementiert werden, deren Objekte dann in einem Tile referenziert werden.
Es ist ratsam die Tiles in einem zweidimensionalen Array oder einer ähnlichen Struktur anzuordnen um einen möglichst simplen und schnellen Zugriff zu gewährleisten. Zu beachten ist, das der Datentyp der Property Coordinates kein 2D Vektor mit Gleitkommazahlen ist, sondern eine Punktstruktur die aus zwei Integer Werten besteht, welche die Indices im Spielfeld-Array widerspiegeln. Die Liste der Entities dient zur Beschleunigung der Kollisionserkennung und des Renderings, mehr dazu später. Diese Klasse kann vererbt und erweitert werden, um benutzerspezifische Logik zu implementieren und somit Beispielsweise in unpassierbare Wände mit Kollision umgewandelt werden.
Die Entity Klasse wird im Vergleich zu den Tiles weitaus komplexer, da diese sich frei über das Spielfeld bewegen können und beim Überschreiten von einem zum nächsten Tile sich in deren Entity Listen ein- und austragen. Eine überlagerbare Update Methode ermöglicht das hinzufügen Benutzerdefinierter Logik, die jeden Frame aufgerufen wird. Darin kann Bewegung, Kollisionserkennung, und auch KI oder eine Input Abfrage implementiert werden. Damit diese Methode in jedem Frame für alle Entities - auch diese, die gerade nicht gerendert werden - aufgerufen wird, benötigt man eine globale Liste mit allen aktiven Entities, die im zentralen Singleton der Engine gespeichert wird (Im Folgenden wird diese Liste als \klammer{LoadedEntities} bezeichnet). Da Collections, wie Listen, nicht modifiziert werden, sollten während über diese iteriert wird, sind zwei weitere Listen nötig: Zum einen \glqq MarkedForSpawn\grqq, in die Entities hinzugefügt werden, die während dem Updaten anderer Entities erstellt wurden. Nach der Iteration durch \klammer{LoadedEntities} wird diese Liste an \klammer{LoadedEntities} angehängt. Zum anderen ist \klammer{MarkedForDeath} eine Liste für Entities die während der Iteration nicht entfernt werden können, aber danach aus \klammer{LoadedEntities} entfernt werden sollen. Ausserdem ist es für viele Spiele, besonders wenn diese aus der Top-Down Perspektive dargestellt werden, notwendig Entities beliebig zu rotieren.
Im Anhang ist ein Codebeispiel für die Entity Klasse beigefügt.
Anzumerken ist dabei, dass ViewDirection und Rotation die gleiche Information in einem anderen Format bereitstellt. Es ist auch möglich die Rotation als gewöhnliche Variable zu definieren und ViewDirection zur Property mit Getter und Setter zu ändern. Da sich die Zugriffssyntax nicht ändert, ist dies aber vorerst nicht relevant und kann später zur Optimierung angepasst werden, je nach dem was öfter verändert und aufgerufen wird. Size ist eine Skalierung und ist optional. Die meisten Spiele benötigen kein dynamisches Vergrößern oder Schrumpfen von Entities, trotzdem ist es ratsam die Engine so dynamisch wie möglich zu gestalten, solange dies keinen großen Einfluss auf die Performance hat.

\subsection{Sprites}

Ein Sprite ist eine Computergrafik, die beliebig positioniert werden kann. Es ist ratsam zusätzlich zu der Referenz auf die Textur einige Metainformationen zu speichern:
\begin{itemize}
	\item{Rotation: Nicht immer soll der Sprite gleich ausgerichtet sein}
	\item{Origin: 2D Vector das das Zentrum des Sprites definiert}
	\item{Scale: Skalierung der Auflösung des Sprites}
	\item{Tint: Einfärbung des Sprites}
	\item{Children: Eine Liste mit Sprites, die an diesen Sprite angehängt werden können}
	\item{Hardpoint: Offset Vector, relevant für den Fall, dass dieses Sprite an ein anderes angehängt ist}
\end{itemize}
Mit diesen Metainformationen ist ein Sprite Objekt weitaus mächtiger als eine einfache Bitmap. Zwar muss zusätzlich zur eigentlichen Bitmap weiterer Arbeitsspeicher für diese Informationen reserviert werden, allerdings ist es äußerst hilfreich diese Werte zur Laufzeit verändern zu können. Somit kann man Beispielsweise, anstatt mehrere Texturen eines Kleidungsstücks, eine einzige laden, und diese dann mit Hilfe der Tint Variable entsprechend einfärben. Es lassen sich auch Hautfarben Generatoren oder andere prozedurale Farbgeneratoren schreiben um diese Variable nicht immer gleich zu befüllen und ein Spiel mit wenig Basisinhalt abwechslungsreicher und spannender wirken zu lassen. Der Origin Vector wird sowohl als initialer Offset benutzt, als auch als Zentrum der Rotation. Wichtig ist es, dass diese Metainformationen als überlagerbare Properties implementiert sind, um die Vererbung der Sprite Klasse zu vereinfachen.

\subsection{Rendering mit 2D Kameras}
Eine Kamera Klasse benötigt mindestens folgende Informationen:
\begin{itemize}
	\item{Location: Ein Vector der das Zentrum der Kamera in Weltkoordinaten definiert}
	\item{Zoom: Ein Faktor für die Größe der Spielelemente auf dem Bildschirm}
	\item{Target: Ein Entity dem die Kamera jeden Frame folgt, kann auch null sein}
\end{itemize}
Im Singleton \klammer{EngineMain} muss eine Variable oder ein Property vorhanden sein, in dem die Kamera, die zum Rendering benutzt wird definiert wird. Jeden Frame wird dann aus dem Zoom der Kamera errechnet welche Größe ein Tile auf dem Bildschirm hat. Es ist wichtig dies auf eine Ganzzahl zu runden, um sogenanntes \klammer{Bleeding}, also erscheinen von ein Pixel weiten Kanten zwischen den Tiles zu verhindern. Aus der Position und der Tilegröße kann daraufhin errechnet werden, welches Tile sich in der oberen linken und der unteren rechten Ecke befindet. Über alle dazwischen liegenden Tiles wird dann iteriert und diese mit einer Sprite Batch gerendert. Mit einem höheren Z-Wert, werden dann alle Entities, beziehungsweise deren Sprites relativ zur Weltkoordinate des Entities, gerendert. Somit wird nur das gerendert, was sich auch innerhalb der Ecken befindet. Allerdings sollte man beachten, dass man für größeren Entities, die sich über mehrere Tiles erstrecken können eventuell pop-in Effekte am Rand auftreten. Um dieses Problem zu umgehen, kann der Rendering Bereich erweitert werden, sodass auch Tiles und deren Entities, die sich außerhalb des Bildschirms befinden, gerendert werden.

\subsection{Animationen}

Animationen können in einer Engine auf verschiedene Arten umgesetzt werden. Die wohl simpelste Art der Animation ist ein Sprite nach Ablauf einer bestimmten Zeit mit einem anderen auszutauschen. Geschieht dies mehrmals in kurzen zeitlichen Abständen, entsteht der Eindruck einer Bewegung.
Innerhalb der Engine kann dies durch eine Klasse gelöst werden, die von der zuvor beschriebenen Sprite Klasse erbt. Diese enthält zwei Arrays, eines das Referenzen auf alle Sprites in der Animation speichert und eines für die entsprechenden zeitlichen Verzögerungen. Die Properties der Textur und sämtlicher Metainformationen, die in der Sprite Klasse definiert sind, werden so überlagert, dass immer die Werte des derzeit relevanten Sprites benutzt werden. Somit kann ein animierter Sprite genauso verwendet werden wie ein regulärer Sprite.
\begin{lstlisting}[caption=Überlagerung einer Property zur Animation,label=lstng:animation]
public override Texture2D Texture {
	get { return SpriteSet[CurrentIndex].Texture; }
	set { SpriteSet[CurrentIndex].Texture = value; }
}
\end{lstlisting}
Der Nachteil bei solchen Animationen ist, dass jede Grafik einzeln erstellt und geladen werden muss. Für rotierende Animationen, wie Beispielsweise die Animation eines Rads, ist es empfehlenswert eine neue Kindklasse von Sprite abzuleiten und die Property der Rotation so umzuschreiben, dass je nach vergangener Zeit eine andere Rotation ausgegeben wird.
\begin{lstlisting}[caption=Überlagerung der Rotation,label=lstng:rotatingAnimation]
public override float Rotation
{
	get
	{
		//Turn Time is the Time one Rotation takes in Milliseconds
		return (float)(RotationOffset + ((long)(Board.Now - CreationMoment).TotalMilliseconds % TurnTime) / (double)TurnTime) * 6.2832f;
	}
	set
	{
		RotationOffset = (double)value;
	}
}
\end{lstlisting}
Ein weiterer Vorteil ist, dass dies eine Stufenlose Animation ist, die sich jeden Frame verändert und somit sehr flüssig wirkt.
[Helikopter Animation mit rotation und mit Spritetausch]
Besonders bei der Animation von Personen oder anderen spielbaren Charaktären kommt das Problem von parallel laufenden Animationen auf. Um mit dem klassischen Animationssystem den Anschein von mehreren gleichzeitigen Aktionen, wie beispielsweise dem Schwingen eines Schwerts während der Laufanimation, müsste man extrem viele verschiedene Grafiken erstellen und laden. Eine Lösung für dieses Problem ist das Aufteilen in einzelne, seperat animierbare Sprites. Diese können mit den Hardpoint und Children Properties der Sprite Klasse zusammengefügt werden und einzeln durch Spritewechsel oder Manipulation der Metainformationen bewegt werden. Eine Armbewegung kann also durch eine Funktion oder ein Skript realisiert werden, das die Rotation Property des Arm-Sprites das am Torso-Sprite befestigt ist verändert. Unabhängig davon können andere Sprites  Ein massiver Vorteil dieser Methode ist die Modularität der Grafikelemente: Beim Austauschen der Grafiken müssen nicht alle Animationen verworfen und neu erstellt werden.
[ARK Animation]
\subsection{Kollisionserkennung}

Ein zentrales Element der meisten 2D Spiele ist die Kollisionserkennung. Die Bereiche, bei deren Überschneidung die Kollisionslogik ausgelöst wird, werden Hitboxen genannt. Es gibt mehrere Möglichkeiten Hitboxen zu implementieren, mit unterschiedlichen vor- und Nachteilen. Die wohl simpelste Variante ist die runde Hitbox. Diese besteht lediglich aus einem Zentrum und einem Radius. Bei der Überprüfung ob sich zwei dieser Hitboxen überschneiden berechnet man die Distanz der beiden Zentren und falls diese geringer als die Summe der beiden Radien ist, kann man sich sicher sein, dass die Hitboxen sich überschneiden. Dies ist äußerst einfach zu implementieren und sehr performant. Für viele Spiele mit runden oder annähernd kreisförmigen Entities, wie Beispielsweise Asteroids, ist dies zudem eine sehr genaue Art der Kollisionserkennung. Anderseits ist diese Art gerade für sogenannte Sidescroller, wie Super Mario, nicht geeignet, da die Spielfiguren eher einem Rechteck als einem Kreis ähneln. Die Diskrepanz zwischen dem sichtbaren aufeinandertreffen zweier Entities und der Kollisionserkennung ist dann so stark, dass der Spieler frustriert oder verwirrt darauf reagieren könnte. Rechteckige, gleich ausgerichtete, Hitboxen werden mit einem 2D Vektor für das Zentrum und vier Gleitkommazahlen, bzw. zwei Vektoren, für die Ausdehnung des Rechtecks in alle Richtungen. Bei der Überprüfung, ob zwei dieser Rechtecke kollidieren, berechnet man, ob die Summe der Ausdehnungen in die entgegen gesetzte Richtung kleiner ist, als der Absolutwert der Distanz zwischen den Zentren in die selbe Richtung. Zum besseren Verständnis folgt nun ein kurzes Codebeispiel:
\begin{lstlisting}[caption=Kollisionserkennung bei unrotierten Rechtecken,label=lstng:collision]
public bool Collision(Hitbox hitboxA, Hitbox hitboxB){
	Vector2 distanceVector = hitboxA.Center-hitboxB.Center;
	Vector2 absoluteDistance = new Vector2(Math.Abs(distanceVector.x),Math.Abs(distanceVector.y))
	bool right = hitboxA.MaxX+hitboxB.MinX > absoluteDistance.x;
	bool left = hitboxA.MinX+hitboxB.MaxX > absoluteDistance.x;
	bool up = hitboxA.MaxY+hitboxB.MinY > absoluteDistance.y;
	bool down = hitboxA.MinY+hitboxB.MaxY > absoluteDistance.y;
	return right||left||up||down;
}
\end{lstlisting}
Dies gilt aber nur wenn das Rechteck nicht rotiert ist, also die Kanten parallel zu den entsprechenden Achsen im Koordinatensystem sind. Um eine rechteckige Hitbox, die Rotation unterstützt zu implementieren, müssen alle vier Eckpunkte in ihrer nicht rotierten Form als Vektor gespeichert werden. Bei einer Rotation, die normalerweise ausgelöst wird wenn das darunter liegende Entity rotiert, werden alle Punkte mithilfe einer entsprechenden Rotationsmatrix transformiert und zwischen gespeichert. Bei einer Überprüfung ob sich zwei dieser Hitboxen überschneiden, wird nun überprüft, ob sich die Kanten, also die Strecken zwischen jeweils zwei Eckpunkten, von einer Hitbox mit einer beliebigen Kante der anderen überschneidet. Die Mathematische Herangehensweise an dieses Problem wäre alle Geradengleichungen der Kanten einer Box mit jeder Kante der anderen gleich zu setzen. Wenn einer der Schnittpunkte innerhalb der definierten Ecken liegt, kann man sich sicher sein, dass sich die Strecken kreuzen und somit eine Kollision vorliegt. Die Implementierung dieser Logik kann in \CSharp unterschiedlich realisiert werden, ein Beispiel befindet sich im Anhang.\newline
Durch das Tile-System in der Engine, können bei der Kollisionserkennung bestimmte Entities bereits als potentielle Kollisionen ausgeschlossen werden. Hat die Hitbox an keiner Stelle eine Ausdehnung, die größer als $\sqrt{2}\, \cdot$ Kantenlänge eines Tiles ist, müssen nur Entities die sich auf dem selben oder den benachbarten acht Tiles befinden auf eine Kollision überprüft werden, ansonsten ist der Bereich in dem auf Kollision überprüft wird entsprechend zu Erweitern.
\subsection{Splats}

Je nach Hardware und genauer Umsetzung der Engine wird die Bildrate bei etwa ein- bis zehntausend Entities unter 60 Bilder pro Sekunde fallen. Um diese massiven Mengen an Entities zu vermeiden, sollte man Objekte die sich nicht mehr bewegen, wie beispielsweise Holzsplitter, auf eine Ebene über den Tiles rendern und dann löschen. Der große Vorteil bei dieser Optimierung ist, dass mehrere kleine Sprites auf eine einzige Grafik gerendert werden können und danach nicht mehr jeden Frame Ressourcen beanspruchen. XNA bietet die Möglichkeit das Render Target einer Spritebatch zu verändern, somit kann nicht nur auf den Bildschirm, sondern auf eine beliebige Bitmap im Arbeitsspeicher gezeichnet werden. Zu beachten ist allerdings, dass ein Render Target nicht gleichzeitig bearbeitet und ausgelesen werden kann um Lesefehler zu vermeiden. Würde also eine Spritebatch eine Grafik zeichnen während genau diese Grafik von einer anderen Spritebatch verändert wird, würde das Spiel abstürzen. Parallelisierung ist dementsprechend nur teilweise möglich. Ein weiteres Problem ist die Menge an Arbeitsspeicher die benötigt wird um das gesamte Spielfeld mit einer riesigen Bitmap zu überziehen. Stattdessen kann zur Tile Klasse eine Property vom Typ \klammer{RenderTarget} hinzugefügt werden. Diese wird mit null initialisiert und erst dann mit einem richtigen RenderTarget befüllt, wenn diese auch benötigt wird. Wichtig ist es, dass dieses initial mit ausschließlich Transparenten Pixeln befüllt ist, da die ursprüngliche Textur des Tiles darunter noch sichtbar sein soll. Somit kann sichergestellt werden, dass für einen minimalen Bereich des Spielfelds eine derartige Leinwand erstellt wird. Das \klammer{RenderTarget} kann wie eine Textur einer SpriteBatch übergeben werden, um den Inhalt auf ein anderes Ziel zu rendern. In der Draw Methode kann dann das Tile mit der ursprünglichen Textur und der Leinwand gerendet werden, wobei zu beachten ist, dass die Leinwand einen höheren Z-Wert haben muss, damit diese nicht von der normalen Textur überdeckt wird.

\section{Zusammenfassung}

Jedes Projekt ist stark unterschiedlich, jedoch sollten Sie mit den Informationen aus diesem Artikel ein solides Grundgerüst mit einer sauberen und stark erweiterbaren Architektur für eine sehr allgemein brauchbare 2D Engine haben. Bestimmte Aspekte, wie das Verarbeiten von Input und die Ausgabe von Sound wurden nicht behandelt, falls Sie in diesen Bereichen Unterstützung benötigen, bietet sich das in den oberen Kapiteln zitierte Buch \klammer{Learning XNA 4.0}, oder die offizielle XNA Dokumentation von Microsoft an. Für den Fall, dass Sie Probleme haben, die beschriebenen Algorithmen zu implementieren, stehen Ihnen im Anhang einige Codebeispiele zur Verfügung. Orientieren Sie sich allerdings nicht zu stark an Paradigmen zur Spiele Entwicklung (auch wenn Sie diese gerade in diesem Artikel gelesen haben), wenn Sie selbst eine bessere Idee haben. Neue Konzepte sind immer eine Bereicherung für ein Spiel und heben dieses von der breiten Masse ab, deshalb ist es nicht ratsam von seinem Konzept abzuweichen, nur um sich Arbeit bei der Implementierung zu sparen.

\section{Anhang}

\begin{lstlisting}[caption=Codeausschnitt aus der Entity Klasse,label=lstng:entity]
public Vector2 Location
{
	get
	{
		return _location;
	}
	set
	{
		if ((int)value.X != coordinates.X || (int)value.Y != coordinates.Y)
		{
			//in this case the entity just entered a new Tile
			Transition(coordinates, new Point((int)value.X, (int)value.Y));
			coordinates.X = (int)value.X;
			coordinates.Y = (int)value.Y;
		}
		_location = value;
	}
}

public Vector2 Offset
{
	get { return new Vector2(_location.X - (int)_location.X, _location.Y - (int)_location.Y); }
}
/// <summary>
/// Returns the X and Y coordinate of the Field it's standing on
/// </summary>
private Point coordinates;  //used for efficient check if the field is supposed to be changed
/// <summary>
/// no need to manually overwrite this, only set it in the constructor once
/// </summary>
private Vector2 _location;  //actual location variable. Use Location to change it

public int Faction = 0;		//Used for various events and hitdetection. Projectiles for instance ignore Entities with the same Faction
public float Size;
public float PixelSize
{
	get { return Size / EngineMain.DEFAULT_FIELD_PIXELS; }
}
public Vector2 ViewDirection;	//View Direction used for Rotation and acceleration
public Vector2 Momentum = new Vector2(0f, 0f);
public Sprite Image;
public bool Dead = false;

public float Rotation {
	get
	{
		return (float)Math.Atan2(ViewDirection.Y, ViewDirection.X);
	}
	set
	{ 
		ViewDirection = new Vector2((float)Math.Cos(value), (float)Math.Sin(value)); 
	}
}

public int X_Coord { get { return (int)this.Location.X; } }
public int Y_Coord { get { return (int)this.Location.Y; } }

//Collision Detection code removed in this example; Impementation may vary depending on your needs

public Entity(Vector2 location)
{
	_location = location;
	coordinates = new Point((int)location.X, (int)location.Y);
	if (!EngineMain.iterating)
	{
		ApplyLocation();
		Board.LoadedEntities.AddFirst(this);
	}
	else
	{
		Board.MarkedForSpawn.AddFirst(this);
	}
}

public void ApplyLocation()
{
	if (coordinates.X >= 0 && coordinates.Y >= 0 && coordinates.X < Board.Map.GetLength(0) && coordinates.Y < Board.Map.GetLength(1))
	{
		Board.Map[coordinates.X, coordinates.Y].Entities.AddFirst(this);
	}
	Location = _location;
}

public virtual void Transition(Point from, Point to)
{
	if (from.X >= 0 && from.Y >= 0 && from.X < Board.Map.GetLength(0) && from.Y < Board.Map.GetLength(1))
	{
		Board.Map[from.X, from.Y].Entities.Remove(this);
	}
	if (to.X >= 0 && to.Y >= 0 && to.X < Board.Map.GetLength(0) && to.Y < Board.Map.GetLength(1))
	{
		//confirmed that the designated field exists
		Board.Map[to.X, to.Y].Entities.AddFirst(this);
	}
	else
	{
		OutOfBounds();//Virtual Method, shall be overwritten to determine what should happen when the entity is out of bounds
	}
}
\end{lstlisting}

\begin{lstlisting}[caption=Board Klasse,label=lstng:board]
public static class Board
{
	public static Random Maprand = new Random((int)DateTime.Now.Ticks);
	public static Random Gamerand = new Random((int)DateTime.Now.Ticks);

	public static LinkedList<Entity> LoadedEntities = new LinkedList<Entity>();
	
	public static LinkedList<Entity> MarkedForDeath = new LinkedList<Entity>();
	public static LinkedList<Entity> MarkedForSpawn = new LinkedList<Entity>();
	public static LinkedList<AI_Manager> AIs = new LinkedList<AI_Manager>();
	public static LinkedList<Splattable> MarkedForSplat = new LinkedList<Splattable>(); 

	public static DateTime Now;	//Gametime

	public static DateTime LastFrameTime;

	public static Tile[,] Map;

	public static bool NotOutOfBounds(int x, int y) { 
		return !(x<0||x>=Map.GetLength(0)||y<0||y>=Map.GetLength(1));
	}
}
\end{lstlisting}

\begin{lstlisting}[caption=Renderlogik für Tiles innerhalb von EngineMain,label=lstng:main]
public void DrawTile()
{
	int max_X = (int)(Screensize.X * (1 / SelectedCamera.Zoom) / DEFAULT_FIELD_PIXELS) + 8;
	int max_Y = (int)(Screensize.Y * (1 / SelectedCamera.Zoom) / DEFAULT_FIELD_PIXELS) + 8;
	int x = 0;
	int y = 0;
	int curX;
	int curY;
	int scaledsize = (int)(SelectedCamera.Zoom * DEFAULT_FIELD_PIXELS);
	Point focused = SelectedCamera.FocusedField;                                             //save Point for performance reasons
	Vector2 offset = SelectedCamera.Offset * SelectedCamera.Zoom * DEFAULT_FIELD_PIXELS;        //save offset for performance reasons
	Tile curTile; //iterated variable
	Rectangle curRect; //Size of painted texture
	while (x < max_X)
	{
		y = 0;
		while (y < max_Y)
		{
			curX = x + (int)TopLeftField.X;
			curY = y + (int)TopLeftField.Y;
			if (curX >= 0 && curY >= 0 && curX < Board.Map.GetLength(0) && curY < Board.Map.GetLength(1))
			{
				curTile = Board.Map[curX, curY];
				if (curTile != null)
				{
					curRect = new Rectangle((int)(x * scaledsize - TopLeftOffset.X) - 4 * scaledsize, (int)(y * scaledsize - TopLeftOffset.Y) - 4 * scaledsize, scaledsize, scaledsize);
					
					spriteBatch.Draw(curTile.Floor.Texture, curRect, null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.2f);
					if (curTile.Structure != null)
					{
						spriteBatch.Draw(curTile.Structure.Texture, curRect, null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.21f);
					}
					else if (curTile.Floor.PermanentCanvas != null) {
						spriteBatch.Draw(curTile.Floor.PermanentCanvas, curRect, null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.21f);
					}
					DrawEntities(curRect.Location, curTile);
				}
			
			}
			y++;
		}
		x++;
	}

}

public void DrawEntities(Point UpperLeftCorner, Field field)
{
	int screencordX;
	int screencordY;
	Texture2D relevantTexture;
	Vector2 curPos;
	Vector2 origin;
	Sprite uppersprite;
	float scale;
	Color tint;
	float prio;
	float prioincrease;
	int layernumber;
	foreach (Entity entity in field.Entities)
	{
		prio = 0.5f;
		prioincrease = 0.1f / entity.getTexturelist().Count;
		layernumber = 0;
		foreach (Sprite sprite in entity.getTexturelist())
		{
			relevantTexture = sprite.Texture;
			//origin = new Vector2(sprite.Scale * entity.Size * sprite.Offset.X, sprite.Scale * entity.Size * sprite.Offset.Y);
			scale = SelectedCamera.Zoom * (entity.Size / relevantTexture.Width) * sprite.Scale;
			
			float rotation = entity.Rotation + (float)(Math.PI / 2);
			
			origin = new Vector2(sprite.Offset.X * entity.Size / (entity.Size / relevantTexture.Width), sprite.Offset.Y * entity.Size / (entity.Size / relevantTexture.Height));
			
			screencordX = UpperLeftCorner.X + (int)((entity.Offset.X * DEFAULT_FIELD_PIXELS * SelectedCamera.Zoom));
			screencordY = UpperLeftCorner.Y + (int)((entity.Offset.Y * DEFAULT_FIELD_PIXELS * SelectedCamera.Zoom));
			curPos = new Vector2(screencordX, screencordY);
			if (sprite.Mothersprite != null)
			{
				LinkedList<Sprite> relevantSprites = new LinkedList<Sprite>();
				uppersprite=sprite.Mothersprite;
				while (uppersprite!=null) {
					relevantSprites.AddFirst(uppersprite);
					uppersprite = uppersprite.Mothersprite;
				}
			
				foreach(Sprite s in relevantSprites){
					rotation += s.Rotation;
					curPos = Vector2.Transform(new Vector2(s.Hardpoint.X * s.Texture.Width * SelectedCamera.Zoom + curPos.X, s.Hardpoint.Y * s.Texture.Height * SelectedCamera.Zoom + curPos.Y), Matrix.CreateTranslation(-(curPos.X), -(curPos.Y), 0) * Matrix.CreateRotationZ(rotation) * Matrix.CreateTranslation(curPos.X, curPos.Y, 0));
				}
				curPos = Vector2.Transform(new Vector2(sprite.Hardpoint.X * relevantTexture.Width * SelectedCamera.Zoom + curPos.X, sprite.Hardpoint.Y * relevantTexture.Height * SelectedCamera.Zoom + curPos.Y), Matrix.CreateTranslation(-(curPos.X), -(curPos.Y), 0) * Matrix.CreateRotationZ(rotation) * Matrix.CreateTranslation(curPos.X, curPos.Y, 0));
			}
			if (sprite.Tintable)
			{
				tint = sprite.Tint;
			}
			else
			{
				tint = Color.White;
			}
			
			spriteBatch.Draw(relevantTexture, curPos, null, tint, rotation + sprite.Rotation, origin, scale, SpriteEffects.None, prio - (layernumber * prioincrease));
			
			layernumber++;
		}
		//maybe a list of textures returned by the animation system? IDK?
	}
}
\end{lstlisting}

\renewcommand{\refname}{\spacedlowsmallcaps{References}} % For modifying the bibliography heading



%----------------------------------------------------------------------------------------

\end{document}