\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Einleitung}{3}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}XNA}{3}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Parallelisiertes Rendering}{4}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Zentrale Elemente einer 2D Engine}{5}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Entity- und Tilesystem}{6}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Sprites}{8}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Rendering mit 2D Kameras}{8}{subsection.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}Animationen}{9}{subsection.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5}Kollisionserkennung}{10}{subsection.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.6}Splats}{12}{subsection.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Zusammenfassung}{13}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Anhang}{13}{section.5}
